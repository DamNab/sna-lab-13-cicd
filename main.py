from flask import Flask
import os

app = Flask(__name__)

@app.route('/')
def index():
    return 'Some text'


app.run(host='0.0.0.0', port=int(os.environ.get("PORT", 60000)))